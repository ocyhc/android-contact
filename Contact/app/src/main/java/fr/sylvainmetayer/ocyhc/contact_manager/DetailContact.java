package fr.sylvainmetayer.ocyhc.contact_manager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class DetailContact extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        Contact contact = null;


        /**
         * Si le bundle n'est pas null, on récupère le contact associé.
         */
        if (b != null) {
            contact = (Contact) i.getExtras().get("CONTACT");
            Log.d("CONTACT", "DETAILS : \n\t" + contact.toString());
        }

        // On récupère les elements de la vue.
        TextView nom_tv = (TextView) findViewById(R.id.identite);
        TextView mail_tv = (TextView) findViewById(R.id.mail);
        TextView phone_tv = (TextView) findViewById(R.id.phoneNumber);
        ImageView image = (ImageView) findViewById(R.id.photoURI);


        // Les boutons
        final FloatingActionButton fab_mail = (FloatingActionButton) findViewById(R.id.mail_button);
        final FloatingActionButton fab_delete = (FloatingActionButton) findViewById(R.id.delete_button);
        final FloatingActionButton fab_call = (FloatingActionButton) findViewById(R.id.call_button);

        // Pour update le contact
        MenuItem update_action = (MenuItem) findViewById(R.id.update_button);

        final Contact monContact = contact;

        // FIXME: 21/03/16 Probleme NPE sur action button

        update_action.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Contact contact = monContact;
                Intent i = new Intent(DetailContact.this, ContactUpdate.class);
                i.putExtra("CONTACT", contact);
                startActivity(i);
                return false;
            }
        });

        // Détails contact à afficher
        final String phone = contact.getFirstPhone();
        final String prenomText = getResources().getString(R.string.nom_prenom) + contact.getName();
        final String mailText = getResources().getString(R.string.mail) + contact.getMails();
        final String phoneText = getResources().getString(R.string.phone) + contact.getPhones();

        // Photo haute qualité
        Uri photo = ContactManager.getPhotoUri(this, Integer.toString(contact.getId()));
        Log.d("PHOTO", photo != null ? photo.toString() : "NULL");

        if (!contact.getMails().isEmpty()) {
            final String mail_adresse = contact.getFirstMail();
            fab_mail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:"));
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mail_adresse});
                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "\n\nEnvoyé depuis ma superbe application de contact !");
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            });
        } else {
            fab_mail.setVisibility(View.GONE);
        }


        fab_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isDeleted = ContactManager.deleteContact(getContentResolver(), monContact);
                MainActivity.adapter.removeFromList(monContact);
                if (isDeleted) {
                    Toast.makeText(getApplicationContext(), "Contact supprimé !", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Contact non supprimé...", Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });

        if (!phone.isEmpty()) {
            fab_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri number = Uri.parse("tel:" + phone);
                    final Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                    startActivity(callIntent);
                }
            });
        } else {
            fab_call.setVisibility(View.GONE);
        }

        if (photo != null) {
            Glide.with(getApplicationContext()).load(photo).into(image);
        }

        if (contact.getPhones().isEmpty()) {
            phone_tv.setVisibility(View.GONE);
        } else {
            phone_tv.setText(phoneText);
        }

        if (contact.getMails().isEmpty()) {
            mail_tv.setVisibility(View.GONE);
        } else {
            mail_tv.setText(mailText);
        }

        if (contact.getMails().isEmpty()) {
            phone_tv.setPadding(0, 0, 0, 200);
        } else {
            mail_tv.setPadding(0, 0, 0, 200);
        }

        nom_tv.setText(prenomText);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
