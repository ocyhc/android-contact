package fr.sylvainmetayer.ocyhc.contact_manager;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Contact implements Parcelable {


    private int id;
    private String name;
    private List<String> phones, mails;
    private String photoURI;

    public Contact(int id, String name, List<String> phones, List<String> mails, String photoURI) {
        this.id = id;
        this.name = name;
        this.phones = new ArrayList<>();
        this.phones.addAll(phones);
        this.mails = new ArrayList<>();
        this.mails.addAll(mails);
        this.photoURI = photoURI;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFirstPhone() {
        return phones.isEmpty() ? "0000000000" : phones.get(0);
    }

    public String getFirstMail() {
        return mails.isEmpty() ? "anonyme@example.fr" : mails.get(0);
    }

    public String getPhones() {
        StringBuilder sb = new StringBuilder();
        for (String s : this.phones) {
            sb.append("\n\t- " + s);
        }
        return sb.toString();
    }

    public String getMails() {
        StringBuilder sb = new StringBuilder();
        for (String s : this.mails) {
            sb.append("\n\t- " + s);
        }
        return sb.toString();
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean hasPhone() {
        return !phones.isEmpty();
    }

    public boolean hasMail() {
        return !mails.isEmpty();
    }

    public String getPhotoURI() {
        return photoURI;
    }

    public void setPhotoUri(String a) {
        this.photoURI = a;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phones=" + phones +
                ", mails=" + mails +
                ", photoURI='" + photoURI + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeStringList(this.phones);
        dest.writeStringList(this.mails);
        dest.writeString(this.photoURI);
    }

    protected Contact(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.phones = in.createStringArrayList();
        this.mails = in.createStringArrayList();
        this.photoURI = in.readString();
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        if (id != contact.id) return false;
        if (name != null ? !name.equals(contact.name) : contact.name != null) return false;
        if (phones != null ? !phones.equals(contact.phones) : contact.phones != null) return false;
        if (mails != null ? !mails.equals(contact.mails) : contact.mails != null) return false;
        return !(photoURI != null ? !photoURI.equals(contact.photoURI) : contact.photoURI != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (phones != null ? phones.hashCode() : 0);
        result = 31 * result + (mails != null ? mails.hashCode() : 0);
        result = 31 * result + (photoURI != null ? photoURI.hashCode() : 0);
        return result;
    }
}
