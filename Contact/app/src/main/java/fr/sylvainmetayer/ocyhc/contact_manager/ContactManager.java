package fr.sylvainmetayer.ocyhc.contact_manager;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de gestion des contacts
 */
public class ContactManager {

    /**
     * @param contentResolver
     * @param contact
     * @return 0=KO, 1=OK, ou le numero du contact
     */
    public static List<String> insertContact(ContentResolver contentResolver,
                                             Contact contact, Bitmap photoContact) {

        // On prépare toutes les requetes.
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        int rawContactID = ops.size();

        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(
                        ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(
                        ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                        contact.getName()).build());

        if (!contact.getMails().isEmpty()) {
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
                            rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, contact.getFirstMail())
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_HOME).build());
        }


        if (!contact.getPhones().isEmpty()) {
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
                            contact.getFirstPhone())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build());
        }

        if (photoContact != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            photoContact.compress(Bitmap.CompressFormat.PNG, 100, baos);
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, baos.toByteArray())
                    .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE).build());
        }

        try {
            // Toutes les requetes créées sont executées
            contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            if (e != null) {
                Log.d("AJOUT", e.getMessage() != null ? e.getMessage() : "Pas de message");
            }
            return null;
        }


        String[] projection = {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
        };

        Cursor cur = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                projection, null, null, ContactsContract.Contacts._ID + " DESC");

        if (cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String photo = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
                Log.d("PHOTO", photo != null ? photo : "Pas de photo");
                Log.d("ID", id);
                ArrayList<String> tmp = new ArrayList<>();
                tmp.add(id);
                tmp.add(photo);
                return tmp;
                //return Integer.parseInt(id);
            }
        }

        return null;
    }

    public static boolean deleteContact(ContentResolver contentResolver,
                                        Contact contact) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        String[] args = new String[]{String.valueOf(contact.getId())};
        ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI)
                .withSelection(ContactsContract.RawContacts.CONTACT_ID + "=?", args).build());
        try {
            contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            Log.d("DELETE", e.getMessage());
            e.printStackTrace();
            return false;
        } catch (OperationApplicationException e) {
            Log.d("DELETE", e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean updateContact(ContentResolver contactHelper, Contact contact) {
        return false;
        //TODO faire la procedure d'update
    }


    /**
     * Obtenir une image haute qualité
     *
     * @param activity
     * @param id
     * @return
     */
    public static Uri getPhotoUri(Activity activity, String id) {
        try {
            Cursor cur = activity.getContentResolver().query(
                    ContactsContract.Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=" + id + " AND "
                            + ContactsContract.Data.MIMETYPE + "='"
                            + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null,
                    null);
            if (cur != null) {
                if (!cur.moveToFirst()) {
                    return null; // no photo
                }
            } else {
                return null; // error in cursor process
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long
                .parseLong(id));
        return Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }

}
