package fr.sylvainmetayer.ocyhc.contact_manager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe permet d'ajouter un contact.
 */
public class AddContact extends AppCompatActivity {

    private EditText identite, mail, phone;
    private ImageView image;
    private Uri photoContact;
    private Bitmap photoBmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        identite = (EditText) findViewById(R.id.form_identite);
        mail = (EditText) findViewById(R.id.form_mail);
        phone = (EditText) findViewById(R.id.form_phone);
        image = (ImageView) findViewById(R.id.form_image);
        photoContact = null;
        photoBmp = null;

        image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });

        Button addContactButton = (Button) findViewById(R.id.add_contact_button);
        addContactButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                addContact();
            }
        });


    }

    private static final int PICK_PHOTO_FOR_AVATAR = 0;

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Log.d("ERREUR", "Erreur! ");
                return;
            }

            photoContact = data.getData();
            Log.d("URI", photoContact.toString());
            try {
                photoBmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(photoContact), null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Glide
                    .with(getApplication())
                    .load(photoContact)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .placeholder(R.drawable.ic_contact)
                    .centerCrop()
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getBaseContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            image.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        }
    }

    /**
     * Une fois que l'utilisateur a rempli les données, on teste et on ajoute
     */
    private void addContact() {

        View focusView = null;

        String identite = this.identite.getText().toString();
        String phone = this.phone.getText().toString();
        String mail = this.mail.getText().toString();

        List<String> phones = new ArrayList<>();
        phones.add(phone);
        List<String> mails = new ArrayList<>();
        mails.add(mail);

        boolean cancel = false;

        if (TextUtils.isEmpty(identite)) {
            this.identite.setError(getString(R.string.error_champ_requis));
            focusView = this.identite;
            cancel = true;
        }

        if (TextUtils.isEmpty(mail)) {
            this.mail.setError(getString(R.string.error_champ_requis));
            focusView = this.mail;
            cancel = true;
        } else if (!isEmailValid(mail)) {
            this.mail.setError(getString(R.string.error_invalid_email));
            focusView = this.mail;
            cancel = true;
        }

        if (cancel) {
            // Une erreur = pas d'ajout et on focus sur l'erreur
            focusView.requestFocus();
        } else {
            Contact c = new Contact(0, identite, phones, mails, photoContact != null ? photoContact.toString() : null);
            List<String> retour = ContactManager.insertContact(getContentResolver(), c, photoBmp);

            if (retour != null) {
                if (Integer.parseInt(retour.get(0)) == 1) {
                    Toast.makeText(getApplicationContext(), "Le contact n'a pas pu être ajouté...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Le contact a été ajouté !", Toast.LENGTH_SHORT).show();
                    c.setId(Integer.parseInt(retour.get(0))); //Afin de récupérer l'id du contact inséré.
                    c.setPhotoUri(retour.get(1)); //Afin de récupérér l'emplacement de la photo du contact
                }
                MainActivity.adapter.appendToList(c);
            } else {
                // Retour = null donc
                Toast.makeText(getApplicationContext(), "Le contact n'a pas pu être ajouté...", Toast.LENGTH_SHORT).show();
            }

            this.finish();
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

}

