package fr.sylvainmetayer.ocyhc.contact_manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.List;

/**
 * Adapter
 */
public class Adapter extends ArrayAdapter<Contact> {
    private LayoutInflater inflater;
    private int layout;
    private List<Contact> objects;

    public Adapter(Context context, int resource, List<Contact> objects) {
        super(context, resource, objects);

        this.objects = objects;
        inflater = LayoutInflater.from(context);
        layout = resource;
    }

    public void appendToList(Contact c) {
        this.objects.add(c);
        this.notifyDataSetChanged();
    }

    public void removeFromList(Contact c) {
        this.objects.remove(c);
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // on récupère l'élément à afficher
        Contact element = getItem(position);

        // classe interne permettant de sauvegarder un lien vers les champs à modifier
        final ViewHolder holder;

        // si l'élément de la liste view n'a pas encore été créé
        if (convertView == null) {

            // on 'inflate' la vue à partir du Layout fourni
            convertView = inflater.inflate(layout, parent, false);

            // on créer un nouvel objet
            holder = new ViewHolder();


            holder.texte = (TextView) convertView.findViewById(R.id.texte);
            holder.image = (ImageView) convertView.findViewById(R.id.image);


            // on attache la sauvegarde à la vue
            convertView.setTag(holder);
        } else {
            // si la vue est déjà créée, on récupère la sauvegarde
            holder = (ViewHolder) convertView.getTag();
        }

        String nom = element.getName();

        holder.texte.setText(nom);

        if (element.getPhotoURI() != null) {
            Glide
                    .with(getContext())
                    .load(element.getPhotoURI())
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .placeholder(R.drawable.ic_contact)
                    .centerCrop()
                    .into(new BitmapImageViewTarget(holder.image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.image.setImageDrawable(circularBitmapDrawable);
                        }
                    });

        } else {
            Glide
                    .with(getContext())
                    .load(R.drawable.ic_contact)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .placeholder(R.drawable.ic_contact)
                    .centerCrop()
                    .into(new BitmapImageViewTarget(holder.image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.image.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        }

        return convertView;
    }

    static class ViewHolder {
        TextView texte;
        ImageView image;

    }

}
