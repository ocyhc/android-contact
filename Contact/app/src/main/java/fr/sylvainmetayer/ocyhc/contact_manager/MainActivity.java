package fr.sylvainmetayer.ocyhc.contact_manager;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    protected static Adapter adapter;
    protected static List<Contact> contacts = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        new LoaderContact(this).execute();
        loadMenu();

    }

    protected void loadMenu() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton add_button = (FloatingActionButton) findViewById(R.id.add_button);
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Merci de patienter avant d'ajouter un contact..", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.update_button) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Méthode qui permet d'afficher les contacts et de définir
     * les listeners une fois le chargement des contacts effectué
     */
    protected void showContacts() {
        ImageView loaderPic = (ImageView) findViewById(R.id.loaderWelcome);
        loaderPic.setVisibility(View.GONE);
        TextView loadertv = (TextView) findViewById(R.id.chargement_int);
        loadertv.setVisibility(View.GONE);

        ListView liste = (ListView) findViewById(R.id.list);
        liste.setVisibility(View.VISIBLE);

        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //Afficher les détails d'un contact.
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact contact = (Contact) parent.getItemAtPosition(position);
                Intent i = new Intent(MainActivity.this, DetailContact.class);
                i.putExtra("CONTACT", contact);
                i.putExtra("POSITION", position);
                startActivity(i);
            }
        });


        FloatingActionButton add = (FloatingActionButton) findViewById(R.id.add_button);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AddContact.class);
                startActivity(i);

            }
        });

        adapter = new Adapter(this, R.layout.contact_simple, contacts);
        liste.setAdapter(adapter);
    }

    /**
     * Traitement long qui permet de récupérer la liste de tous les contacts
     *
     * @return List<Contact> : liste qui correspond à la liste des contacts présent dans le téléphone
     */
    protected List<Contact> getAllContacts() {

        List<Contact> contacts = new ArrayList<>();

        // Données du contacts
        String contact_id = null;
        String name = null;
        List<String> phoneNumber = null;
        List<String> mails = null;
        String photoURI = null;

        // Au total, on a 3 requêtes à faire pour récupérer les données des contacts
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;

        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;

        // Pour éviter de faire une requete enorme., même si ça reste long
        String[] projection = {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
        };

        ContentResolver contentResolver = getContentResolver();

        // On récupère toutes les données par ordre alphabétique
        Cursor cursor = contentResolver.query(CONTENT_URI, projection, null, null, DISPLAY_NAME + " ASC");

        if (cursor != null && cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                photoURI = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));

                phoneNumber = new ArrayList<>();

                // Si jamais il y a des numeros de téléphones, on les récupère tous
                if (hasPhoneNumber > 0) {
                    String[] projectionPhone = {
                            NUMBER
                    };
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, projectionPhone, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);

                    while (phoneCursor.moveToNext()) {
                        String tmpNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        phoneNumber.add(tmpNumber);
                    }
                    phoneCursor.close();

                }

                // On récupère les adresses mails du contacts
                mails = new ArrayList<>();
                String[] projectionMail = {
                        DATA
                };
                Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, projectionMail, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);


                while (emailCursor != null && emailCursor.moveToNext()) {
                    String emailTmp = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                    mails.add(emailTmp);

                }
                emailCursor.close();

                // On a désormais toutes les informations concernant notre contact, on va le créer.
                Contact contactTmp = new Contact(Integer.parseInt(contact_id), name, phoneNumber, mails, photoURI);
                Log.d("CONTACT", contactTmp.toString());
                contacts.add(contactTmp);
            }

        }
        if (cursor != null) {
            cursor.close();
        }

        return contacts;
    }


    private class LoaderContact extends AsyncTask<Void, Integer, List<Contact>> {

        private Activity activity;

        public LoaderContact(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected List<Contact> doInBackground(Void... params) {
            List<Contact> contacts = new ArrayList<>();

            // Données du contacts
            String contact_id = null;
            String name = null;
            List<String> phoneNumber = null;
            List<String> mails = null;
            String photoURI = null;

            int compteur = 0;

            // Au total, on a 3 requêtes à faire pour récupérer les données des contacts
            Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
            Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;

            String _ID = ContactsContract.Contacts._ID;
            String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
            String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
            String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
            String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
            String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
            String DATA = ContactsContract.CommonDataKinds.Email.DATA;

            // Pour éviter de faire une requete enorme., même si ça reste long
            String[] projection = {
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER,
                    ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
            };

            ContentResolver contentResolver = getContentResolver();

            // On récupère toutes les données par ordre alphabétique
            Cursor cursor = contentResolver.query(CONTENT_URI, projection, null, null, DISPLAY_NAME + " ASC");

            if (cursor != null && cursor.getCount() > 0) {

                while (cursor.moveToNext()) {

                    contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                    name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                    int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                    photoURI = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));

                    phoneNumber = new ArrayList<>();

                    // Si jamais il y a des numeros de téléphones, on les récupère tous
                    if (hasPhoneNumber > 0) {
                        String[] projectionPhone = {
                                NUMBER
                        };
                        Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, projectionPhone, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);

                        while (phoneCursor.moveToNext()) {
                            String tmpNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                            phoneNumber.add(tmpNumber);
                        }
                        phoneCursor.close();

                    }

                    // On récupère les adresses mails du contacts
                    mails = new ArrayList<>();
                    String[] projectionMail = {
                            DATA
                    };
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, projectionMail, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);


                    while (emailCursor != null && emailCursor.moveToNext()) {
                        String emailTmp = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        mails.add(emailTmp);

                    }
                    emailCursor.close();

                    // On a désormais toutes les informations concernant notre contact, on va le créer.
                    Contact contactTmp = new Contact(Integer.parseInt(contact_id), name, phoneNumber, mails, photoURI);
                    Log.d("CONTACT", contactTmp.toString());
                    contacts.add(contactTmp);
                    compteur = compteur + 1;
                    publishProgress(compteur);
                }

            }
            if (cursor != null) {
                cursor.close();
            }

            return contacts;
        }


        private void updateLoading(String value) {
            TextView tv = (TextView) activity.findViewById(R.id.chargement_int);
            StringBuilder sb = new StringBuilder();
            sb.append(getResources().getString(R.string.chargement));
            sb.append(" : ");
            sb.append(value);
            sb.append(" % ...");

            tv.setText(sb.toString());
        }

        protected void onPreExecute() {

            // Pour éviter de "casser" le traitement
            lockScreenOrientation();
            loadMenu();

            // On cache la liste et on affiche le loader et le texte de chargement
            Log.d("STATE", "TRAITEMENT COMMENCE");
            activity.setContentView(R.layout.activity_main);

            this.updateLoading("0");

            // Chargement du GIF animé !
            ImageView loaderPic = (ImageView) activity.findViewById(R.id.loaderWelcome);
            loaderPic.setVisibility(View.VISIBLE);
            ImageView imageView = (ImageView) findViewById(R.id.loaderWelcome);
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
            Glide.with(activity).load(R.raw.giphy).into(imageViewTarget);

            ListView liste = (ListView) activity.findViewById(R.id.list);
            liste.setVisibility(View.INVISIBLE);

        }

        protected void onPostExecute(List<Contact> resultat) {
            unlockScreenOrientation();
            Log.d("STATE", "TRAITEMENT FINI");
            contacts = resultat;
            showContacts();
        }


        private void lockScreenOrientation() {
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }

        private void unlockScreenOrientation() {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }

        List<Integer> compteur_total = new ArrayList<>();

        protected void onProgressUpdate(Integer... values) {
            for (int value : values) {
                compteur_total.add(value);
            }

            Integer cent = 100;
            Integer toConvert = compteur_total.get(compteur_total.size() - 1);
            Integer nb = compteur_total.size() + 30; // FIXME: 21/03/16 Problème de compteur.
            Integer afficher = (toConvert * 100) / nb;

            Log.d("VALUE VALUE[0]", Integer.toString(values[0]));
            Log.d("VALUE TO_CONVERT", Integer.toString(toConvert));
            Log.d("VALUE CONVERT", Integer.toString(afficher));
            Log.d("VALUE CPT_SIZE", Integer.toString(compteur_total.size()));

            this.updateLoading(Integer.toString(afficher));
        }

    }

}

/*
Liens pour préparer le PDF
https://androidresearch.wordpress.com/2013/05/10/dealing-with-asynctask-and-screen-orientation/
 */